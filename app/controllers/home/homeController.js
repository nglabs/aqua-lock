﻿(function () {
    
    var HomeController = function ($scope, $location, Settings, Tips) {
        $scope.settings = Settings;

        if (isNaN(parseInt(Settings.sizeOfHousehold)) ||
            isNaN(parseInt(Settings.averageWaterBill)) ||
            isNaN(parseInt(Settings.bathUseFrequency)) ||
            isNaN(parseInt(Settings.showerUseFrequency)) ||
            isNaN(parseInt(Settings.toothbrushUseFrequency)) ||
            isNaN(parseInt(Settings.dishwasherUseFrequency)) ||
            isNaN(parseInt(Settings.handdishwashUseFrequency)) ||
            isNaN(parseInt(Settings.washingmachineUseFrequency)) ||
            isNaN(parseInt(Settings.toiletUseFrequency))) {
            console.log("Some data is missing")
            $location.path('/settings');
        }

        var monthlyUsageForHousehold = 0;

        switch (Settings.sizeOfHousehold) {
            case 1:
                monthlyUsageForHousehold = 5.5;
                break;
            case 2:
                monthlyUsageForHousehold = 9.2;
                break;
            case 3:
                monthlyUsageForHousehold = 11.3;
                break;
            case 4:
                monthlyUsageForHousehold = 13.75;
                break;
            case 5:
                monthlyUsageForHousehold = 15.2;
                break;
            case 6:
                monthlyUsageForHousehold = 16.7;
                break;
            default:
                monthlyUsageForHousehold = 16.7;
                break;
        }

        // average use

        var bf = $scope.settings.bathUseFrequency * 0.077 * 4.333333;
        var sf = $scope.settings.showerUseFrequency * 0.036 * 4.333333;
        var tbf = $scope.settings.toothbrushUseFrequency * 0.018 * 4.333333;
        var df = $scope.settings.dishwasherUseFrequency * 0.02 * 4.333333;
        var hf = $scope.settings.handdishwashUseFrequency * 0.009 * 4.333333;
        var wf = $scope.settings.washingmachineUseFrequency * 0.071 * 4.333333;
        var tf = $scope.settings.toiletUseFrequency * 0.009 * 4.333333;

        var totalMonthlyUsage = bf + sf + tbf + df + hf + wf + tf;

        var dailyUsage = totalMonthlyUsage / moment().daysInMonth();

        var todayDate = new Date().getDate();
        
        var currentUsage = todayDate * dailyUsage;

        var saving = (((monthlyUsageForHousehold * 1.94) - (totalMonthlyUsage * 1.94))).toFixed(2);

        if (saving > 0) {
            $scope.tracking = "You are on track to meet your target!";
        } else {
            $scope.tracking = "You are not on track to meet your target";
        }

        $scope.monthlyUsage = monthlyUsageForHousehold;
        $scope.saving = saving;
        $scope.savingDisplay = saving < 0 ? saving * -1 : saving;

        var remainingPer = ((monthlyUsageForHousehold - currentUsage) / monthlyUsageForHousehold) * 100;

        $('.water').animate({ 'height': remainingPer + '%' }, 600);
    };
    
    angular.module('myApp').controller('HomeController', HomeController);

}());