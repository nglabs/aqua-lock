﻿(function () {
    
    var SettingsController = function ($scope, $location, Settings) {

        $scope.formData = Settings;

        $scope.updateSettings = function () {

            var bf = $scope.formData.bathUseFrequency * 0.077 * 4.333333;
            var sf = $scope.formData.showerUseFrequency * 0.036 * 4.333333;
            var tbf = $scope.formData.toothbrushUseFrequency * 0.018 * 4.333333;
            var df = $scope.formData.dishwasherUseFrequency * 0.02 * 4.333333;
            var hf = $scope.formData.handdishwashUseFrequency * 0.009 * 4.333333;
            var wf = $scope.formData.washingmachineUseFrequency * 0.071 * 4.333333;
            var tf = $scope.formData.toiletUseFrequency * 0.009 * 4.333333;

            var projectedWaterUsage = bf + sf + tbf + df + hf + wf + tf;

            localStorage.setItem('sizeOfHousehold', $scope.formData.sizeOfHousehold);
            localStorage.setItem('averageWaterBill', $scope.formData.averageWaterBill);
            localStorage.setItem('bathUseFrequency', $scope.formData.bathUseFrequency);
            localStorage.setItem('showerUseFrequency', $scope.formData.showerUseFrequency);
            localStorage.setItem('toothbrushUseFrequency', $scope.formData.toothbrushUseFrequency);
            localStorage.setItem('dishwasherUseFrequency', $scope.formData.dishwasherUseFrequency);
            localStorage.setItem('handdishwashUseFrequency', $scope.formData.handdishwashUseFrequency);
            localStorage.setItem('washingmachineUseFrequency', $scope.formData.washingmachineUseFrequency);
            localStorage.setItem('toiletUseFrequency', $scope.formData.toiletUseFrequency);
            localStorage.setItem('projectedWaterUsage', $scope.monthlyUsage);
            $location.path('/home');
        }
    };
    
    angular.module('myApp').controller('SettingsController', SettingsController);

}());