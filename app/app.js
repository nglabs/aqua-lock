﻿/// <reference path="../Scripts/angular.min.js" />

(function () {
    var tips = [
        'A shower timer can encourage you to spend less time in the shower which will reduce your water use.',
            'Promptly repair any leaks in and around your taps. (One leak can waste several thousand litres of water per year.)',
            'When hand-washing dishes, never run water continuously. Wash dishes in a partially filled sink and then rinse them using the spray attachment on your tap.',
            'If you have an electric dishwasher, use it only to wash full loads, and use the shortest cycle possible. Many dishwashers have a conserver/water-miser cycle.',
            'When brushing your teeth, turn the water off while you are actually brushing. Use short bursts of water for cleaning your brush. (This saves about 80% of the water normally used.)',
            'When washing or shaving, partially fill the sink and use that water rather than running the tap continuously. (This saves about 60% of the water normally used.) Use short bursts of water to clean razors.',
            'Use either low-flow shower heads or adjustable flow-reducer devices on your shower heads. (They reduce flow by at least 25%.)',
            'You can reduce water usage by 40% to 50% by installing low-flush toilets.',
            'Wash only full loads in your washing machine.',
            'Use the shortest cycle possible for washing clothes, and use the "suds-saver" feature if your machine has one.',
            'Lawns and gardens require only 5 millimetres of water per day during warm weather. Less is needed during spring, autumn, or cool weather.',
            'Place a cistern displacement device in your toilet cistern to reduce the volume of water used in each flush. You can get one of these from your water provider. ',
            'Take a shorter shower. (Shower can use anything between 6 and 45 litres per minute)',
            'Always use full loads in your washing machine and dishwasher – this cuts out unnecessary washes in between.',
            'Fix a dripping tap. A dripping tap can waste 15 litres of water a day, or 5,500 litres of water a year.',
            'Install a water butt to your drainpipe and use the water collected to water your plants, clean your car and wash your windows.',
            'Fill a jug with tap water and place this in your fridge. This will mean you do not have to leave the cold tap running for the water to run cold before you fill your glass.',
    ];

    var app = angular.module('myApp', ['ngAnimate', 'ui.router', 'angularMoment']);

    setTimeout(
       function asyncBootstrap() {
           angular.bootstrap(document, ['myApp']);
       }, (4 * 1000)
   );

    app.config(function ($stateProvider, $urlRouterProvider) {
        var viewBaseDir = '/app/views/';

        $stateProvider
            .state('home', {
                url: '/home',
                controller: 'HomeController',
                templateUrl: viewBaseDir + 'home/home.html',
            })
            .state('settings', {
                url: '/settings',
                controller: 'SettingsController',
                templateUrl: viewBaseDir + 'settings/settings.html',
            })
            .state('articles', {
                url: '/articles',
                controller: 'ArticlesController',
                templateUrl: viewBaseDir + 'articles/articles.html',
            })
            .state('info', {
                url: '/info',
                controller: 'InfoController',
                templateUrl: viewBaseDir + 'info/info.html',
            });

        $urlRouterProvider.otherwise('/home');
    });

    app.service('Settings', function () {
        return {
            sizeOfHousehold: parseInt(localStorage.getItem("sizeOfHousehold")),
            averageWaterBill: parseFloat(localStorage.getItem("averageWaterBill")),
            bathUseFrequency: parseInt(localStorage.getItem("bathUseFrequency")),
            showerUseFrequency: parseInt(localStorage.getItem("showerUseFrequency")),
            toothbrushUseFrequency: parseInt(localStorage.getItem("toothbrushUseFrequency")),
            dishwasherUseFrequency: parseInt(localStorage.getItem("dishwasherUseFrequency")),
            handdishwashUseFrequency: parseInt(localStorage.getItem("handdishwashUseFrequency")),
            washingmachineUseFrequency: parseInt(localStorage.getItem("washingmachineUseFrequency")),
            toiletUseFrequency: parseInt(localStorage.getItem("toiletUseFrequency")),
            projectedWaterUsage: parseFloat(localStorage.getItem("projectedWaterUsage")),
        };
    });

    app.service('Tips', function () {
        return tips;
    });

    app.directive(
        "mAppLoading",
        function ($animate) {

            return({
                link: link,
                restrict: "C"
            });

            function link(scope, element, attributes) {
                $animate.enabled(true);
                $animate.leave(element.children()).then(
                    function cleanupAfterAnimation() {
                        element.remove();
                        scope = element = attributes = null;
                    }
                );
            }
        }
    );
    
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $(".tip").html(tips[getRandomInt(0, tips.length - 1)]);

}());