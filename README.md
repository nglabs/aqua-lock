### Summary ###
Aqua Lock is a small client-side-only project demonstrating insights into water consumption for individual households using mock data. Create a profile by providing basic information of your household, that is used to determine typical water usage which becomes a monthly target. For a proof of concept, water usage data has been mocked, and the idea is to use real water usage sensor data in the future.
### Third-party libraries used: ###
* AngularJS
* JQuery
* Bootstrap
* Moments JS